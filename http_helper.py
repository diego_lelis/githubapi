import requests
from requests.adapters import HTTPAdapter
from requests.packages.urllib3.util.retry import Retry


def send_session_request(
        retries=4,
        backoff_factor=0.2,
        status_forcelist=(500, 502, 504),
        session=None):
    """ Helper that creates a session to try to
    connect for multiple times with the server
    :param retries: Number of tries
    :param backoff_factor: Incremental interval between tries
    :param status_forcelist: Force list Status
    :param session: Existing session that it is not being used.
    :return: session """
    session = session or requests.Session()
    retry = Retry(
        total=retries,
        read=retries,
        connect=retries,
        backoff_factor=backoff_factor,
        status_forcelist=status_forcelist,
    )
    adapter = HTTPAdapter(max_retries=retry)
    # session.mount('http://', adapter)
    session.mount('https://', adapter)
    return session
