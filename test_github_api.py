from unittest import TestCase
from github_api import GithubAPI
from github_api import GithubRepository
import responses
import json
import math


class TestGithubAPI(TestCase):
    @responses.activate
    def setUp(self):
        with open("test_data.json") as data:
            self.test_data = json.load(data)
        # Necessary because update_search_lim is runned when class is created
        self.create_rate_limits_response()
        self.github_api = GithubAPI()

    def create_rate_limits_response(self):
        json_resp = self.test_data["limits_response"]
        responses.add(
            method=responses.GET,
            url="https://api.github.com/rate_limit",
            json=json_resp,
            status=200
        )

    def create_search_by_language_resp(
            self,
            test_data_name,
            language, page_num):
        json_resp = self.test_data[test_data_name]
        params = {
            "language": language,
            "page_num": page_num
        }
        url = self.github_api.tmp_str_search_by_language.substitute(params)
        responses.add(
            method=responses.GET,
            url=url,
            json=json_resp,
            status=200
        )

    def create_search_by_python_response(self, repo_count, start_page=1):
        page_quant = math.ceil(repo_count/self.github_api.items_per_page)
        for i in range(start_page, start_page + page_quant):
            self.create_search_by_language_resp(
                f"search_python_page_{i}_res",
                "Python",
                i
            )

    def test_rate_limits_begin(self):
        self.assertDictEqual(
            self.test_data["limits_response"],
            self.github_api.limits
        )

    def test_search_repositories_rate_limits_repo_exceeded(self):
        repo_count = 8*30
        commits_per_repo = 15
        self.assertRaises(
            Exception,
            self.github_api.search_repositories_async,
            repo_count,
            commits_per_repo
        )

    def search_by_language_helper(self, language):
        self.create_search_by_python_response(1, 1)
        res = self.github_api.search_by_language_sync(language)
        return res

    @responses.activate
    def test_search_by_language_python(self):
        expec_repos = self.test_data["search_python_page_1_res"]["items"]
        expec_repos = list(map(GithubRepository, expec_repos))
        res = self.search_by_language_helper("Python")
        self.assertSequenceEqual(res, expec_repos)

    def create_commits_response(self, test_data_name, repo, page_num):
        json_resp = self.test_data[test_data_name]
        repo = repo
        url = f"https://api.github.com/repos/{repo}/commits?page={page_num}"
        # print("test_url: ", url)
        responses.add(
            method=responses.GET,
            url=url,
            json=json_resp,
            status=200
        )

    def create_django_commits_response(self, repo_count, start_page=1):
        page_count = math.ceil(repo_count/self.github_api.items_per_page)
        for page in range(start_page, start_page + page_count):
            self.create_commits_response(
                f"get_commit_repo_django_page_{page}_res",
                "django/django",
                page
            )

    @responses.activate
    def test_get_commits_repo(self):
        commits_per_repo = 45
        self.create_django_commits_response(commits_per_repo)
        repo_json = {
                "full_name": "django/django",
                "stargazers_count": 39690,
            }
        repository = GithubRepository(repo_json)
        commit_arr = self.test_data["get_commit_repo_django_page_1_res"]
        commit_arr = [c["commit"]["author"]["email"] for c in commit_arr]
        self.assertSequenceEqual(repository.load_repo_commits(), commit_arr)

    def gen_expec_repos_python(self, repo_count, start_page=1):
        page_count = math.ceil(repo_count/self.github_api.items_per_page)
        expec_repos = []
        for i in range(start_page, start_page + page_count):
            repo_aux_list = self.test_data[
                f"search_python_page_{i}_res"
            ]["items"]
            repo_aux_list = [
                GithubRepository(repo_json) for repo_json in repo_aux_list
            ]
            expec_repos.extend(repo_aux_list)
        return expec_repos[:repo_count]

    @responses.activate
    def test_get_repos_async(self):
        repo_count = 4 * 31
        self.create_search_by_python_response(repo_count)
        expec_repos = self.gen_expec_repos_python(repo_count)
        repos = self.github_api.run_get_repositories_async(
            language="Python",
            repo_count=repo_count,
        )
        self.assertSequenceEqual(expec_repos, repos)

    def create_expected_commit_response(self, repo_count, start_page=1):
        page_count = math.ceil(repo_count/self.github_api.items_per_page)
        expec_commit = []
        for i in range(start_page, start_page + page_count):
            commit_aux = self.test_data[f"get_commit_repo_django_page_{i}_res"]
            commit_aux = [c["commit"]["author"]["email"] for c in commit_aux]
            expec_commit.extend(commit_aux)
        return expec_commit

    @responses.activate
    def test_get_commits_async(self):
        commits_per_repo = 125
        self.create_django_commits_response(commits_per_repo)
        expec_commit = self.create_expected_commit_response(
            commits_per_repo, start_page=1
        )
        repo_json = {
                "full_name": "django/django",
                "stargazers_count": 39690,
            }
        repo_list = [GithubRepository(repo_json), GithubRepository(repo_json)]
        self.github_api.load_get_commits_async(
            repo_list,
            commits_per_repo

        )
        self.assertCountEqual(
            repo_list[0].author_email_list_commits,
            expec_commit
        )
        self.assertCountEqual(
            repo_list[1].author_email_list_commits,
            expec_commit
        )

    @responses.activate
    def test_search_repositories_async(self):
        repo_count = 4 * 31
        commits_per_repo = 35
        self.create_search_by_python_response(repo_count)
        expected_repo = self.gen_expec_repos_python(repo_count)
        repositories = self.github_api.search_repositories_async(
            repo_count,
            commits_per_repo,
            load_commits=False
        )
        self.assertEqual(repositories, expected_repo)

    def test_ordering_repo_authors(self):
        repo_json = {
            "full_name": "django/django",
            "stargazers_count": 39690,
        }
        repo = GithubRepository(repo_json)
        repo.authors_count.update({
            "a@ab.com.br": 85,
            "a@aa.com.br": 85,
            "a@accca.com.br": 150
        }
        )

        expect_authors_count = [
            {"email": "a@accca.com.br", "number_of_commits": 150},
            {"email": "a@aa.com.br", "number_of_commits": 85},
            {"email": "a@ab.com.br", "number_of_commits": 85}
        ]

        repo = repo.format_dict_response()
        # print(*repo["authors"], sep="\n")
        self.assertSequenceEqual(expect_authors_count, repo["authors"])
