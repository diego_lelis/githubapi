certifi==2018.11.29
chardet==3.0.4
idna==2.8
python-decouple==3.1
requests==2.21.0
responses==0.10.5
six==1.12.0
urllib3==1.24.1
