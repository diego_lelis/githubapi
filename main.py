from github_api import GithubAPI


def process_repositories(repo_count, commits_per_repo, language="Python"):
    github_api = GithubAPI()

    repositories = github_api.search_repositories_async(
        repo_count,
        commits_per_repo,
        language,
    )
    print(repositories)
    print(type(repositories))
    return repositories


if __name__ == "__main__":
    repo_count = 25
    commits_per_repo = 60
    language = "Python"
    process_repositories(
        repo_count,
        commits_per_repo,
        language
    )
