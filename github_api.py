from requests.auth import HTTPBasicAuth
from decouple import config
from string import Template
from http_helper import send_session_request
from collections import Counter
import asyncio
import concurrent.futures
import math
import json


def check_error(res):
    if res.status_code != 200:
        raise ConnectionError(
            f"""There was an error accessing Github:
                Code: {res.status_code}
                message: {res.json()['message']}"""
        )


class GithubRepository:
    """
    Class that interacts with the repository resource on the Github API
    """
    def __init__(self, repo_json, auth={}):
        """
        :param repo_json: JSON received from github
        :param auth: Authentication parameter that it
        can increase the number of allowed requests
        """
        self.tmp_str_get_commit_repo = \
            Template(f"{GithubAPI.BASE_URL}repos/$repo_name/" +
                     f"commits?page=$page_num"
                     )
        self.repository_name = repo_json["full_name"]
        self.stargazers_count = repo_json["stargazers_count"]
        self.authors_count = Counter()
        self.author_email_list_commits = []
        self.auth = auth

    def __eq__(self, other):
        """
        Custom comparison between repositories, used mainly for testing
        :param other: Another GithubRepository instance
        :return:
        """
        if self.repository_name == other.repository_name \
                and self.stargazers_count == other.stargazers_count\
                and self.authors_count == other.authors_count:
            return True
        return False

    def __str__(self):
        return f"{self.repository_name} | Stars: {self.stargazers_count}"

    def load_repo_commits(self, page_num=1):
        """
        Loads the commits of the repository
        :param page_num: Number of the page to send on the request
        :return: Array with the e-mails of all the commit authors
        """
        data = {
            "repo_name": self.repository_name,
            "page_num": page_num
        }
        url = self.tmp_str_get_commit_repo.substitute(data)
        sess = send_session_request().get(
            url,
            headers=GithubAPI.DEFAULT_HEADER,
            auth=self.auth
        )
        check_error(sess)
        commit_arr_author = sess.json()
        commit_arr_author = [
            c["commit"]["author"]["email"] for c in commit_arr_author
        ]
        self.author_email_list_commits.extend(commit_arr_author)
        sess.close()
        return commit_arr_author

    def count_commits(self, commit_per_repo, rmv_author_email_lst=True):
        """
        Uses the array with authors emails to a create a Counter object
        :param commit_per_repo: This value will slice the array
        :param rmv_author_email_lst: If True, removes
        the array with authors emails to save memory
        """
        self.authors_count = Counter(
            self.author_email_list_commits[:commit_per_repo]
        )
        if rmv_author_email_lst:
            self.author_email_list_commits = []

    def format_dict_response(self):
        """
        Formats class info to the desired format
        :return: Dict with desired structure
        """
        authors = [
                {
                    "email": email,
                    "number_of_commits": self.authors_count.get(email)
                 }
                for email in self.authors_count
            ]
        authors.sort(
            key=lambda author: (
                -author["number_of_commits"],
                author["email"])
        )
        return {
            "repository_name": self.repository_name,
            "stargazers_count": self.stargazers_count,
            "authors": authors
        }


class GithubAPI:
    """
    Functions to interact with the github API.
    If you set your credentials on the .env file,
    it will increase number of allowed requests.
    PS: .env file is included in .gitignore
    """
    BASE_URL = "https://api.github.com/"
    DEFAULT_HEADER = {
        "Accept": "application/vnd.github.v3+json"
    }
    MAX_WORKERS = 20  # Maximum of main parallel requests

    def __init__(self):
        self.repositories = []
        self.search_url = f"{self.BASE_URL}search/"
        self.tmp_str_search_by_language = Template(
            f"{self.search_url}repositories" +
            f"?q=language:$language&page=$page_num"
        )
        self.url_limits = f"{self.BASE_URL}rate_limit"
        self.limits = {}
        self.auth = {}
        self.generate_auth()
        self.update_limits_start()
        # Number of items per page on search and commits
        self.items_per_page = 30

    def check_can_search_repo(self,
                              repo_count,
                              commits_per_repo
                              ):
        """
        Checks if there is enough requests to do the requisition received.
        Raises an exception it is not enough
        :param repo_count: Number of repositories
        :param commits_per_repo: Number of commits per repository
        :return:
        """
        search_page_quant = math.ceil(repo_count / self.items_per_page)
        commit_page_per_repo = math.ceil(
            commits_per_repo / self.items_per_page
        )
        total_commit_page = commit_page_per_repo * self.items_per_page
        # print("total_commit_page: ", total_commit_page)
        search_max_page = self.limits["resources"]["search"]["remaining"]
        commit_max_page = self.limits["resources"]["core"]["remaining"]
        # print(search_page_quant > search_max_page)
        # print("commit_max_page: ", commit_max_page)
        # print("total_commit_page: ", total_commit_page)
        if search_page_quant > search_max_page\
                or total_commit_page > commit_max_page:
            error = f"""Invalid combination of number of repositories and commits.
            The current limits are:
            {search_max_page*self.items_per_page} repositories
            and {commit_max_page*self.items_per_page} commits"""
            user = config("USER", None)
            password = config("PASSWORD", None)
            if not (user and password):
                error += f"""\nTry to add your credentials on
                            .env  filesto increase limits"""
            raise Exception(error)

    def search_repositories_async(
            self,
            repo_count,
            commits_per_repo,
            language="Python",
            load_commits=True
    ):
        """
        Main function responsible to perform repository search on Github.
        This functions orchestrates others to
        allow asynchronous communication with Github
        :param repo_count: Number of repositories to process
        :param commits_per_repo: Number of commits per repo
        :param language: Language to search
        :param load_commits: If False, does not load commits, only repositories
        :return: Array with objects on the desired
        format and ordered accordingly
        """
        self.check_can_search_repo(repo_count, commits_per_repo)
        repositories = self.run_get_repositories_async(
            language=language,
            repo_count=repo_count,
        )
        if not load_commits:
            return repositories

        self.load_get_commits_async(repositories, commits_per_repo)

        for repository in repositories:
            repository.count_commits(commits_per_repo)

        repositories = [
            repository.format_dict_response() for repository in repositories
        ]
        repositories.sort(
            key=lambda repo: (
                -repo["stargazers_count"],
                repo["repository_name"]
            )
        )
        return json.dumps(repositories)

    def generate_auth(self):
        """
        Modifies the authentication used for send requests if credential
        values are found on the environment.
        """
        user = config("USER", None)
        password = config("PASSWORD", None)
        if user and password:
            self.auth = HTTPBasicAuth(username=user, password=password)

    def update_limits_start(self):
        """
        Checks the api limits
        """
        sess = send_session_request().get(self.url_limits, auth=self.auth)
        check_error(sess)
        self.limits = sess.json()
        sess.close()

    def update_limits_res(self, res, resource):
        """
        Updates the search limits, recommended to
        be used on synchronous requests
        :param res: Response with the headers that indicate search limits
        :param resource: Specifies the cateogory that it shoud be updated
        """
        remaining = self.limits["resources"][resource]["remaining"]
        limit = self.limits["resources"][resource]["limit"]
        reset_time = self.limits["resources"][resource]["reset"]
        self.limits["resources"][resource]["remaining"] = res.headers.get(
            "X-RateLimit-Remaining", remaining - 1
        )
        self.limits["resources"][resource]["limit"] = res.headers.get(
            "X-RateLimit-Limit", limit
        )
        self.limits["resources"][resource]["reset"] = res.headers.get(
            "X-RateLimit-Reset", reset_time
        )

    def update_search_limits_res(self, res):
        """Updates search limits when synchronous request is received"""
        self.update_limits_res(res, "search")

    def check_limits(self, resource):
        """Checks limits generic for synchronous requests"""
        if not self.limits["resources"][resource]["remaining"]:
            limit = self.limits["resources"][resource]["limit"]
            raise Exception(f"{limit} Github {resource} calls used")

    def check_can_search(self):
        """Check search limits with syncronous requests"""
        self.check_limits("search")

    def search_by_language_sync(
            self,
            language,
            page_num=1,
            sync=False,
            raw=False):
        """
        Searches repositories by language synchronously
        :param language: Language to search
        :param page_num: Page number
        :param sync: Informs if it is being used in synchronous context,
        if yes, updates limits as requests are sent.
        :param raw: If True returns raw data without processing
        :return: Array with repositories JSON data
        """
        self.check_can_search()
        params = {
            "language": language,
            "page_num": page_num
        }
        url = self.tmp_str_search_by_language.substitute(params)
        sess = send_session_request().get(
            url,
            headers=self.DEFAULT_HEADER,
            auth=self.auth)
        check_error(sess)
        if sync:
            self.update_search_limits_res(sess)
        repo_arr = sess.json()["items"]
        if not raw:
            repo_arr = [GithubRepository(repo_json, self.auth)
                        for repo_json
                        in sess.json()["items"]
                        ]
        sess.close()
        return repo_arr

    def update_commit_limits_res(self, res):
        """Updates commits limits for synchronous
        requisitions using response
        """
        self.update_limits_res(res, "core")

    def run_get_repositories_async(self, language, repo_count, start_page=1):
        """
        Executes multiple Asynchronous requisitions to obtain repositories
        :param language: Language to search
        :param repo_count: Number of Repositories
        :param start_page: Starting page
        :return: Repository array with all the desired repositories.
        """
        loop = asyncio.get_event_loop()
        all_repos = loop.run_until_complete(
            self.get_repositories_async(
                language=language,
                repo_count=repo_count,
                start_page=1,
            )
        )
        return all_repos

    async def get_repositories_async(self, language, repo_count, start_page=1):
        """
        Creates ThreadPool to execute requisitions to obtain repositories
        Change GithubAPI.MAX_WORKERS to increase or
        decrease number of parallel requests
        :param language: Language to search
        :param repo_count: Number of repositories
        :param start_page: Starting page
        :return: Array with desired repositories with correct requested size
        """
        page_quant = math.ceil(repo_count/self.items_per_page)
        all_repos = []
        with concurrent.futures.ThreadPoolExecutor(
                    max_workers=self.MAX_WORKERS) as executor:
            loop = asyncio.get_event_loop()
            futures = [
                loop.run_in_executor(
                    executor,
                    self.search_by_language_sync,
                    language,
                    i
                )
                for i in range(start_page, start_page+page_quant)
            ]
            for repo_group in await asyncio.gather(*futures):
                all_repos.extend(repo_group)
        return all_repos[:repo_count]

    def load_get_commits_async(
            self,
            repo_list,
            commit_repo_count,
            start_page=1):
        """
        Executes multiple Asynchronous requisitions to obtain
        commits from repositories
        :param repo_list: List of repositories to load commits
        :param commit_repo_count: Number of commits per repository
        :param start_page: Starting page
        """
        loop = asyncio.get_event_loop()
        loop.run_until_complete(
            self.get_commits_async(
                repo_list,
                commit_repo_count,
                start_page
            )
        )

    async def get_commits_async(
            self,
            repo_list,
            commit_repo_count,
            start_page=1):
        """
        Creates ThreadPool to execute requisitions to obtain commits
        from repositories Change GithubAPI.MAX_WORKERS to increase
        or decrease number of parallel requests
        :param repo_list: List of repositories to load commits
        :param commit_repo_count: Number of commits per repository
        :param start_page: Starting page
        """
        page_quant = math.ceil(commit_repo_count / self.items_per_page)
        with concurrent.futures.ThreadPoolExecutor(
                max_workers=self.MAX_WORKERS) as executor:
            loop = asyncio.get_event_loop()
            futures = [
                loop.run_in_executor(
                    executor,
                    repository.load_repo_commits,
                    i
                )
                for repository in repo_list or self.repositories
                for i in range(start_page, start_page+page_quant)
            ]
            for commit_page_auth_email in await asyncio.gather(*futures):
                pass
