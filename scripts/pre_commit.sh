#!/usr/bin/env bash

echo "Pre-commit hook"
./scripts/run_tests.sh

# $? accesses result from last command
if [[ $? -ne 0 ]]; then
 echo "Tests didn't passed, commit failed."
 exit 1
fi