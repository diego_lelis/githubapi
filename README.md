## Description

github api that counts commits per repository for a specific programming language.

Implemented with [GitHub API](https://developer.github.com/v3/)

- #Details

- Asynchronous requests used to get info from the API.
- .env file can hold user credentials to increase requests limit, see example.env to copy format.
- To increase the number of parallel requests (run faster), you can change the GithubAPI.MAX_WORKERS value.
- If the user tries to access more info than the limit provided by github it will raise an error.
- Python 3.6 used to develop.

- #Testing and running the application

### Running

    virtualenv -p python3 myenv && source myenv/bin/activate # Optional
    pip install -r requirements.txt
    python main.py

### Testing

    virtualenv -p python3 myenv && source myenv/bin/activate # Optional
    pip install -r requirements.txt
    python -m unittest test_github_api
